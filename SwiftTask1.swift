let firstName = "Rashid"
let secondName = "Safarov"
let age = 21
let height = 175
let weight = 60
let hobby = "Guitar"

print(
	"First name: \(firstName)\n" +
	"Second name: \(secondName)\n" +
	"Age: \(age)\n" +
	"Height: \(height)\n" +
	"Weight: \(weight)\n" +
	"Hobby: \(hobby)"
)